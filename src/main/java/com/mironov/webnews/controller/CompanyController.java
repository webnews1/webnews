package com.mironov.webnews.controller;

import com.mironov.webnews.data.Company;
import com.mironov.webnews.dto.CompanyDTO;
import com.mironov.webnews.exception.CompanyNotFoundException;
import com.mironov.webnews.repository.CompanyRepository;
import com.mironov.webnews.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("v1/companies")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private CompanyRepository companyRepository;

    /**
     *
     * @return  list of all companies
     */
    @GetMapping("")
    public List<CompanyDTO> findCompanies() {
        return companyService.findCompanies().stream()
                .sorted(Comparator.comparingInt(Company::getId))
                .map(CompanyDTO::new)
                .collect(Collectors.toList());
    }

    /**
     *
     * @param id    company ID
     * @return  company with specified ID
     */
    @GetMapping("/{id}")
    public CompanyDTO findCompany(@PathVariable Integer id) {
        return new CompanyDTO(companyRepository.findById(id).orElseThrow(() -> new CompanyNotFoundException(id)));
    }

    /**
     *
     * @param companyDTO    id, name of a company
     * @return  created company
     */
    @PostMapping("")
    @PreAuthorize("hasRole('MANAGER')")
    public CompanyDTO createCompany(@RequestBody CompanyDTO companyDTO) {
        return new CompanyDTO(companyRepository.save(new Company(companyDTO.getId(), companyDTO.getName())));
    }

    /**
     * updates the company with specified ID
     * @param companyDTO    name of a company
     * @param id    id of a company
     * @return  updated company
     */
    @PutMapping("/{id}")
    @PreAuthorize("hasRole('MANAGER')")
    public CompanyDTO updateCompany(@RequestBody CompanyDTO companyDTO, @PathVariable Integer id) {

        return companyRepository.findById(id)
                .map(company -> {
                    company.setName(companyDTO.getName());
                    return new CompanyDTO(companyRepository.save(company));
                })
                .orElseThrow(() -> new CompanyNotFoundException(id));
    }

    /**
     * deletes the company with specified ID
     * @param id    id of a company
     */
    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('MANAGER')")
    void deleteCompany(@PathVariable Integer id) {
        companyRepository.deleteById(id);
    }
}
