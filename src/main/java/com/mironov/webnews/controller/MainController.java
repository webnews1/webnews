package com.mironov.webnews.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class MainController {

    /**
     *
     * @return application short description
     */
    @GetMapping("")
    public String appDescription() {
        return "The application is for users and companies";
    }
}
