package com.mironov.webnews.dto;

import com.mironov.webnews.data.Company;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CompanyDTO {

    public CompanyDTO(Company company) {
        this.id = company.getId();
        this.name = company.getName();
    }

    private Integer id;
    private String name;
}
