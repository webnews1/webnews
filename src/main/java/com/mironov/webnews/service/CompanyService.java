package com.mironov.webnews.service;

import com.mironov.webnews.data.Company;
import com.mironov.webnews.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    public List<Company> findCompanies() {
        return companyRepository.findAll();
    }
}
