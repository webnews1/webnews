package com.mironov.webnews.service;

import com.mironov.webnews.data.Company;
import com.mironov.webnews.repository.CompanyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class CompanyServiceTest {

    private static final List<Company> COMPANIES = Arrays.asList(
            new Company(2, "Google"), new Company(1, "IBM")
    );

    @Mock
    private CompanyRepository companyRepository;

    @InjectMocks // auto inject companyRepository
    private final CompanyService companyService = new CompanyService();

    @BeforeEach
    void setMockOutput() {
        when(companyRepository.findAll()).thenReturn(COMPANIES);
    }

    @DisplayName("Mock companyRepository, find companies")
    @Test
    void testFindCompanies() {
        assertEquals(COMPANIES, companyService.findCompanies());
    }

}
